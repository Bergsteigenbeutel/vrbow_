﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Notch : MonoBehaviour
{
    public OVRGrabber rHand;
    public OVRGrabber lHand;
    public Transform notch;

    private GameObject arrow;
    private Rigidbody arrowRigidbody;
    private CapsuleCollider arrowCollider;

    private bool isArrowAttached = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (isArrowAttached)
        {
            float distance = Vector3.Distance(rHand.transform.position, lHand.transform.position);
            float strength = Mathf.Clamp(distance, 0f, 0.5f);
            arrow.transform.localPosition = new Vector3(0f, 0f, -(strength) + 0.42f);

            if (OVRInput.Get(OVRInput.RawAxis1D.RHandTrigger) <= 0.1f)
            {
                //Abschuss                
                ArrowFlight arrowScript = arrow.GetComponent<ArrowFlight>();
                arrowScript.shootArrow(strength);
                isArrowAttached = false;
            }
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.transform.name == "Shaft" && !isArrowAttached)
        {
            arrow = collider.transform.parent.parent.gameObject;
            OVRGrabbable arrowGrabbable = arrow.GetComponent<OVRGrabbable>();
            if (rHand.grabbedObject == arrowGrabbable);
            {
                attachArrowToBow(arrow);
            }
        }
    }

    private void attachArrowToBow(GameObject arrow)
    {
        OVRGrabbable grabbableArrow = arrow.GetComponent<OVRGrabbable>();
        rHand.ForceRelease(grabbableArrow);

        arrowCollider = arrow.GetComponent<CapsuleCollider>();
        arrowCollider.enabled = false;

        arrowRigidbody = arrow.GetComponent<Rigidbody>();
        arrowRigidbody.isKinematic = true;

        arrow.transform.parent = notch;
        arrow.transform.localPosition = new Vector3(0f, 0f, 0.42f);
        arrow.transform.localRotation = new Quaternion(0f, 0f, 0f, 0f);

        isArrowAttached = true;
    }

    public bool ArrowAttached()
    {
        return isArrowAttached;
    }
}