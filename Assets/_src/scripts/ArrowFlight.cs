﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowFlight : MonoBehaviour
{
    private Rigidbody arrowRigidbody;
    private CapsuleCollider arrowCollider;
    private Vector3 prevPosition;

    void Start()
    {
        arrowRigidbody = GetComponent<Rigidbody>();
        arrowCollider = GetComponent<CapsuleCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!arrowRigidbody.isKinematic && prevPosition != transform.position)
        {
            Vector3 currentDirection = transform.position - prevPosition;
            transform.rotation = Quaternion.LookRotation(currentDirection, transform.up);
        }

        prevPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    }

    public void shootArrow(float strength)
    {
        Vector3 localPos = transform.localPosition;
        localPos.z += .2f;

        transform.localPosition = localPos;
        transform.parent = null;

        arrowCollider.enabled = true;

        arrowRigidbody.isKinematic = false;
        arrowRigidbody.AddRelativeForce(0f, 0f, 8f * strength, ForceMode.Impulse);
    }
}