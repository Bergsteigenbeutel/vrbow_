﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;


public class VrMovement : MonoBehaviour
{
    public OVRCameraRig CameraRig;
    public CharacterController character;
    public Transform playerBody;
    public Transform groundCheck;

    private Vector2 vLeftHandStick;
    private Vector2 vRightHandStick;
    public float rotationSpeed = 100f;
    public bool snapRotation;
    private bool resetRotate = true;
    public float movementSpeed = 5f;
    private Vector3 move;
    private Vector3 velocity;
    private float gravity = -9.81f;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    private bool isGrounded;
    private float jumpHight = 3f;

    

    //Muss da bleiben lol
    public event Action CameraUpdated;
    public event Action PreCharacterMove;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Call VR Mapping Functions
        CharacterFollowHeadset();
        //RotatePlayerToHMD();

        //Gravity
        velocity.y += gravity * Time.deltaTime;
        character.Move(velocity * Time.deltaTime);

        //GroundCheck
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        if(isGrounded && velocity.y < 0)
        {
            velocity.y = -2f; 
        }

        //Get Input from Controller ThumbSticks
        vLeftHandStick = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick) * rotationSpeed * Time.deltaTime;
        vRightHandStick = OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick) * rotationSpeed * Time.deltaTime;

        //Rotate PlayerBody
        if (!snapRotation)
        {
            transform.RotateAround(new Vector3(CameraRig.centerEyeAnchor.transform.position.x, 0, CameraRig.centerEyeAnchor.transform.position.z), Vector3.up, vRightHandStick.x);
        }
        else
        {
            if ((vRightHandStick.x > 0.2f || vRightHandStick.x < -0.2f) && resetRotate)
            {
                if (vRightHandStick.x > 0.2f)
                {
                    playerBody.Rotate(Vector3.up * 45f);
                }
                else
                {
                    playerBody.Rotate(Vector3.up * -45f);
                }

                resetRotate = false;
            }

            if (vRightHandStick.x < 0.1f && vRightHandStick.x > -0.1f)
            {
                resetRotate = true;
            }
        }

        //Move PlayerBody
        move = transform.right * vLeftHandStick.x + playerBody.forward * vLeftHandStick.y;
        character.Move(move * movementSpeed * Time.deltaTime);

        // Jump
        if(isGrounded && OVRInput.Get(OVRInput.Button.One))
            velocity.y = Mathf.Sqrt(jumpHight * -2f * gravity);

    }

    void CharacterFollowHeadset()
    {
        character.height = CameraRig.centerEyeAnchor.transform.position.y;
        Vector3 capsuleCenter = transform.InverseTransformPoint(CameraRig.centerEyeAnchor.transform.position);
        character.center = new Vector3(capsuleCenter.x, character.height / 2 + character.skinWidth / 2, capsuleCenter.z);
        groundCheck.transform.SetPositionAndRotation(new Vector3(character.transform.position.x, character.transform.position.y, character.transform.position.z), new Quaternion(0f, 0f, 0f, 0f));
    }

    void RotatePlayerToHMD()
    {
        Transform root = CameraRig.trackingSpace;
        Transform centerEye = CameraRig.centerEyeAnchor;

        Vector3 prevPos = root.position;
        Quaternion prevRot = root.rotation;

        transform.rotation = Quaternion.Euler(0.0f, centerEye.rotation.eulerAngles.y, 0.0f);

        root.position = prevPos;
        root.rotation = prevRot;
    }
}
