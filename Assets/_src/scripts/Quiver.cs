﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quiver : MonoBehaviour
{

    public GameObject arrowPrefab;
    public OVRGrabber rHand;
    private bool isActive;
    public Notch notch;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(isActive && (OVRInput.Get(OVRInput.RawAxis1D.RHandTrigger) >= 0.1f) && (rHand.grabbedObject == null) && !notch.ArrowAttached())
        {
            GameObject arrow = Instantiate(arrowPrefab);
            arrow.transform.position = rHand.transform.position;
            arrow.GetComponent<Rigidbody>().useGravity = true;
            isActive = false;
        }
    }

    private void OnTriggerEnter(Collider rHand)
    {
        if (rHand.transform.name == "CustomHandRight" && !isActive)
        {
            isActive = true;
        }
    }

    private void OnTriggerExit(Collider rHand)
    {
        if(rHand.transform.name == "CustomHandRight" && isActive)
        {
            isActive = false;
        }
    }
}
