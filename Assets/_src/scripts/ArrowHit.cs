﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowHit : MonoBehaviour
{
    public LayerMask[] hitLayer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision hitobject)
    {
        Rigidbody arrowRig = GetComponent<Rigidbody>();
        arrowRig.isKinematic = true;
        transform.position = hitobject.contacts[0].point;
        transform.parent = hitobject.transform;
    }
}
